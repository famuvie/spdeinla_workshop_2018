# Accounting for the spatio-temporal spread of a fungal invasion

Facundo Muñoz

Ash-dieback is an invasive fungal disease of ash trees characterised by leaf loss and crown dieback in infected trees.
First detected in Poland in the early 1990s, it rapidly progressed throughout Europe causing large numbers of deaths and threatening today the survival of the species.

Using annual observations from 2010 to 2014 within a field experiment located in northern France, I was asked to assess the genetic (co)variation associated with two different symptoms: _Crown Dieback_ (CD) and _Collar Lesion_ (CL)
However, separating the progress of the disease from the genetic effects was of paramount importance for the estimation accuracy.

I modelled the regionalized relative exposure to pathogen agent using a latent spatio-temporal effect constructed as the tensor product of a _Matérn_ spatial process and a exchangeable temporal structure using the SPDE approach for the spatial component and the `group` feature of INLA.

This approach allowed to leverage all the available data to identify families with higher resistance, and a association between resistance and early-flushing.
Furthermore, the high estimated heritabilities imply that breeding is feasible and the disease is manageable.
