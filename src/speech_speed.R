library(tidyverse)
library(xkcd)
loadfonts()
# download.file("http://simonsoftware.se/other/xkcd.ttf", dest="~/Downloads/xkcd.ttf", mode="wb")
# font_import("~/Downloads/")
stopifnot("xkcd" %in% fonts())

set.seed(123456)
data.frame(
  speed = seq(0, 5, len = 10)
) %>% 
  mutate(
    noise = rnorm(length(speed), sd = .1),
    transm = 2*speed/3 - (speed/4)**2,
    noisy_transm = transm + noise,
    noisy_speed = 2*speed/3 + noise,
    rec = speed*exp(-speed),
    noisy_rec = rec + noise
  ) %>% 
  ggplot(aes(speed, transm)) +
  # geom_line(alpha = .3) +
  # geom_line(aes(y = noisy_rec), alpha = .3) +
  geom_smooth(se = FALSE, color = "black") +
  geom_smooth(aes(y = rec), se = FALSE, color = "black") +
  # xkcdline() +
  labs(x = "Speech rate", y = "Information") +
  xkcdaxis(c(0,5), c(0, 1)) +
  annotate(
    "text", x=1.8, y = .75,
    label = "How much\nyou say",
    family="xkcd"
  ) +
  annotate(
    "text", x=3.3, y = .25,
    label = "How much the\naudience gets",
    family="xkcd"
  ) +
  theme(
    axis.text = element_blank(),
    axis.ticks = element_blank()
  )

ggsave("speech_rate.png", width = 6, height = 4)

