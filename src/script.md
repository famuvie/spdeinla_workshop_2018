## 1. Title page

Good morning. Feel free to browse these slides online using this link.

I will present a study I conducted a few years ago, while working as a post-doc in a unit of forest genetics at INRA.


## 2. 

It was in collaboration with Arnaud Dowkiw, a forest geneticist at INRA who works in the genetic resistance to pathogens of poplars and ash trees.


## 3. Ash-dieback

Ash-dieback is an invasive fungal disease of ash trees.
It was first detected in Poland in the early 1990s, and progressed  rapidly throughout Europe causing large numbers of deaths and threatening today the survival of the species.

## 4. Symptoms

It is characterised by leaf loss and crown die-back in infected trees.
It is also frequently (but not always) associated with lesions at the base of the trunk. However, it is unclear (or it was, at the time) whether this is caused by the same pathogen or by some other opportunistic organisms.


## 5. Goals

The main goal of the study was to determine whether there is variation in the resistance or the tolerance to the disease at a __genetic level__, so that forest managers can control the disease by breeding these resistant genotypes.


## 6. Dataset

We had annual observations of the status of the trees within a field experiment located in northern France, with respect to both symptoms.

CD was measured for five years in a categorical scale at five levels of severity, measuring the approximate percentage of defoliation.
CL was measured for two years as a percentage of the circumference affected.

Notice that there is a sustained global increase in the disease along time, together with some spatial autocorrelation. If you look closely, the disease seems to progress from the south-west corner for CD, and from the opposite corner for CL.

This means that we need to account for differences in exposure in order to compare the response of the trees.

I remember when Arnaud approached me and told me, "so, you are a specialist in spatial statistics? cool! I have some data for which I would like to get rid of the spatial autocorrelation. It was my first week as a post-doc at INRA, and I thought: "this guy wants to add some spatial effect? piece of cake!" Sure! I said, we can work it out in a couple of days.
It turned out he had not only spatial, but spatio-temporal data, for two related outcomes, one categorical and the other a proportion with lots of zeros.


## 7. Some modelling decisions

I decided to start by modelling the two symptoms separately since there were only two years worth of observations for CL and the spatial patterns seemed to be different.

And yes, I have to confess I used a Gaussian model for the categorical outcome CD. In my defense, I can say that I compared the results with an ordered-logistic model, without the spatial effect and for the last year only, and the genetic estimates were consistent. 

Finally, since more than half of the observations of CL were zero I used a mixture of a Binomial process that explained the probability of infection, and a Gamma model for the severity of symptoms conditional on infection.
This had the appeal of reflecting possibly two different biological processes.


## 8. Model for (T)CD. Fixed effects.

The model for CD included fixed effects of the Year and the Bud-flush precocity (which is a categorical covariate). Here I show the posterior mean effects and 95% HPD intervals. This captures mainly the consistent increase in the global mean along time.


## 9. Model for (T)CD. Genetic effect.

Then we have the __genetic__ effect, which represented the individual genetic contribution to the response. It is a structured random effect at individual level, with a relationship matrix A which is determined from the kinship of individuals.

These were the quantities of interest. These are posterior estimates at a family level and it shows that there is significant variation across families.


## 10. Model for (T)CD. Spatio-temporal effect.

Finally, here is the posterior mean of the spatio-temporal effect, which is another random effect with a structured matrix Q that depends on a temporal and a spatial autocorrelation parameters.

We can see that there have been a couple of hotspots in the south-west corner, and a few safer spots in the north and the east.


## 11. Spatio temporal structure

Specifically, Q is given by the tensor product of a Matérn spatial process, and a exchangeable temporal structure.

In INLA, such a term is specified with an SPDE object, and a _group_ which is an index for the years.

For identifiability, we need to add a sum-to-zero constraint for the spatial component.


## 12. Priors

Another critical aspect for preserving identifiability is to use sensible priors on the hyperparameters. Specially the range parameter of the spatial term. Since we want to capture environmental effects at the scale of the field.


## 13. Model for CL

For the collar lesion we only had data for two years, and more than half of them were zeros.
So I split the model into two processes. The first explains the probability p of a response of 0. Thus, of not getting infected.
The second process, conditional to being infected, explains the extent of the collar lesions with a Gamma distribution.


## 14. Latent models

For both processes I used the same latent structure, with year-effects, spatio-temporal effects and genetic effects, independently of each other.


## 15. Posterior mean spatio-temporal effect for CL

These are the resulting posterior mean spatio-temporal effects for the binary and continuous processes plus the effect of the year.

Notice how the probability of remaining asymptomatic drops in the second year, while the severity increases.

Otherwise, the spatial patterns remain similar in both years for both processes, suggesting that trees in the southern side were generally safer.
Which is the opposite of what we had with CD.

Furthermore, the spatial patterns are much smoother.

These results strongly support the hypothesis that the processes causing the symptoms in the crown and in the trunk are different. CD is caused by spores spread by the wind, while CL is most likely caused by another pathogen that diffuses on the soil.


## 16. Genetic correlations

These scatter plots represent the marginal posterior mean individual genetic values for each of the tree components.
The correlations are not very strong, but they suggest a positive association between tolerance to crown-defoliation and probability of zero symptoms at the trunk and a negative association with the severity of collar lesions conditional to infection.


## 17. Conclusions

In summary, this approach allowed to compare genotypes controlling for differences in exposure.

We could identify the most resilient families and individuals within families, and quantify the heritability of the traits, which allows to compute the expected reduction in losses.

Finally, the differences in the spatio-temporal patterns strongly support the hypothesis of different causes and pathways for both symptoms.


## 18. Improvements

While I am quite satisfied with the model, and I truly think that it answers the scientific questions, as a statistician I would be happier with a couple of improvements.

First, with a proper ordinal model for CD, of course.

But also modelling the genetic correlations explicitly in a joint model for both symptoms, as well as across the binary and continuous components of CL.

And Finally, accounting for the increase not only in mean, but also in variance along time.


## 19. Thanks

If you are interested, I prepared an R-package with the data used in this study with the code for reproducing the results in the paper.
