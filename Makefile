## Produce a pdf version
## It didn't work well. MathJax was not rendered.
## Usint manual printing to pdf from Chromium instead.
## public/FMunoz_Ash.pdf: public/index.html
##	Rscript -e "library(webshot); webshot(\"$<\", \"$@\")"


public/index.html: src/FMunoz_Ash.html
	cp -r src/img public
	cp -r src/libs public
	cp src/FMunoz_Ash.html public/index.html
	cp src/cirad.css public/
	cp src/macros.js public/
